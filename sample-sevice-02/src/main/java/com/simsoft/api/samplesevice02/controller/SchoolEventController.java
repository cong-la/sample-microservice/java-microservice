package com.simsoft.api.samplesevice02.controller;

import com.simsoft.api.samplesevice02.model.SchoolEvent;
import com.simsoft.api.samplesevice02.repository.SchoolEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/school")
public class SchoolEventController {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @GetMapping(path = "/event")
    public ArrayList<SchoolEvent> findAll() {
        return this.schoolEventRepository.findAll();
    }

    @GetMapping(path = "/{schoolId}/event")
    public ArrayList<SchoolEvent> findBySchoolId(@PathVariable("schoolId") int schoolId) {
        return this.schoolEventRepository.findBySchoolId(schoolId);
    }

    @GetMapping(path = "/event/{id}")
    public SchoolEvent findById(@PathVariable("id") int id) {
        return this.schoolEventRepository.findById(id);
    }

    @PostMapping(path = "/event/add")
    public SchoolEvent save(@RequestBody SchoolEvent schoolEvent) {
        return this.schoolEventRepository.save(schoolEvent);
    }
}
